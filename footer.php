<div class="pt-5 pb-5">
    <div class="col-md-10 offset-md-1 col-10 offset-1">
        <div class="row">
            <div class="col-md-2 offset-md-4 col-6 text-right">
                <img class="w-100" src="img/logo-footer.png" alt="logo footer canada">
            </div>
            <div class="col-md-2 offset-md-0 col-6">
                <img class="w-100" src="img/collegecanada.jpg" alt="college canada">
            </div>

            <div class="text-center col-md-12 pt-5">
                <a class="redes pr-1" target="_blank" href="https://www.facebook.com/tufuturoencanada/"> <i class="fab fa-facebook-f"></i></a>
                <a class="redes pl-1" target="_blank" href="https://www.instagram.com/tufuturoencanada/"><i class="fab fa-instagram"></i></a>
            </div>      
        </div>      
    </div>
</div>        
<footer>
    <div class="text-center">
        <h5>© Copyright 2019 / Tu futuro en Canadá  |  <a href="aviso-de-privacidad">Aviso de privacidad</a></h5>
    </div>
</footer>


<button id="scrollTop"><i class="qode_icon_font_awesome fa fa-arrow-up "></i></button>




<!--Start of Tawk.to Script (0.3.3)-->
<script>
var Tawk_API=Tawk_API||{};
var Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d8cd2c26c1dde20ed03a0a9/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script (0.3.3)--><!-- WhatsHelp.io widget -->
<script>
    (function () {
        var options = {
            whatsapp: "525576675372", // WhatsApp number
            call_to_action: "Escríbenos", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget --><link href="https://fonts.googleapis.com/css?family=Roboto:400" rel="stylesheet" property="stylesheet" type="text/css" media="all">

		<script>
		if(typeof revslider_showDoubleJqueryError === "undefined") {
			function revslider_showDoubleJqueryError(sliderID) {
				var err = "<div class='rs_error_message_box'>";
				err += "<div class='rs_error_message_oops'>Oops...</div>";
				err += "<div class='rs_error_message_content'>";
				err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
				err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' ->  'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
				err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
				err += "</div>";
			err += "</div>";
				jQuery(sliderID).show().html(err);
			}
		}	
</script>

</body>
</html>






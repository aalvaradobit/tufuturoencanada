
   <?php 
        $title =  'Foro tu futuro en Canadá, expertos para estudiar y migrar a Canadá';
        $description = 'Tu Futuro en Canadá reúne a especialistas del mercado de estudio y migración a Canadá, a través de un foro para vivir en Canadá.' ;
        $keywords = 'tu futuro en canadá, instituciones educativas en canadá, foto tu futuro en canadá';
        include('header.php');
   ?>

<section class="main bgLightGray">
    <div class="relative">
        <img class="w-100" src="img/titulo-somos.jpg" alt="titulo somos">
        <h1 class="red absolute somosQ col-md-6 offset-md-3">quiénes somos</h1>
    </div>

    <div class="col-md-10 offset-md-1 pt-5">
        <div class="row">
            <div class="col-md-7 text-justify">
                <p>Tu Futuro en Canadá es una marca que reúne a diferentes especialistas del mercado de estudio y migración a Canadá, con la intención de crear el primer y mejor foro para que las personas puedan cumplir su sueño de estudiar, trabajar y migrar en Canadá.</p>
                <h2 class="red pt-5 text-center">Valores</h2>
                <div class="row pt-5 text-center">
                    <div class="col-md-4">
                        <h3>Expertise</h3>
                        <p>Contamos con todos los especialistas con una amplia experiencia que se te asesorará para cumplir tu sueño en Canadá.</p>
                    </div>
                    <div class="col-md-4">
                        <h3>Honestidad</h3>
                        <p>Trabajamos con transparencia mostrando cada detalle del proceso.</p>
                    </div>
                    <div class="col-md-4">
                        <h3>Responsabilidad</h3>
                        <p>Cuidamos cada detalle para darte la tranquilidad de que el proceso será eficaz.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-5 pt-4 pb-5">
                <img class="w-100" src="img/tu-futuro-en-canada-we.jpg" alt="Cn Tower">
            </div>
        </div>
    </div>
    <h2 class="red text-center pb-5">TODO LO QUE NECESITAS SABER</h2>
    <div class="row text-center no-gutters">
        <div class="col-md-3">
            <img class="somosIcon" src="img/atencion-personal.png" alt="atencion personal">
            <h2 class="red p-3" class="red">TRATO 100% PERSONALIZADO</h2>
        </div>
        <div class="col-md-3">
            <img class="somosIcon" src="img/plan.png" alt="plan">
            <h2 class="red p-3">PROGRAMAS COMPLETAMENTE A TU MEDIDA DE ACUERDO A TUS EXPECTATIVAS</h2>
        </div>
        <div class="col-md-3">
            <img class="somosIcon" src="img/financiamiento.png" alt="financiamiento">
            <h2 class="red p-3">FINANCIAMIENTO</h2>
        </div>
        <div class="col-md-3">
            <img class="somosIcon" src="img/trato-directo.png" alt="trato directo">
            <h2 class="red p-3">TRATO DIRECTO CON LOS REPRESENTANTES DE LAS ESCUELAS</h2>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>
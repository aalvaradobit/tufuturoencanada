<?php
	session_start();
	$captcha_response = htmlspecialchars($_POST['recaptcha_response']);
	$recaptcha_site_secret = '6LfFycIUAAAAAB3Ol6XCISNiXuruxTYdUqjrYJkv';
	$curl = curl_init();
	$captcha_verify_url = "https://www.google.com/recaptcha/api/siteverify";
	curl_setopt($curl, CURLOPT_URL,$captcha_verify_url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, "secret=".$recaptcha_site_secret."&response=".$captcha_response);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$captcha_output = curl_exec($curl);
	curl_close ($curl);
	$decoded_captcha = json_decode($captcha_output);
	$captcha_status = $decoded_captcha->success; // store validation result to a variable.
	if($captcha_status === FALSE){
		$_SESSION["error"] = 2;
		header('Location: '.$_SERVER['HTTP_REFERER']);
	}else{

		
		$httpProtocol = 'https://';
		$host = $_SERVER['SERVER_NAME'];
      $url = '/';
    
		if(isset($_POST["nombre"]) && isset($_POST["correo"]) && isset($_POST["mensaje"])){
			date_default_timezone_set("America/Mexico_City");
        	setlocale(LC_TIME, 'es_ES.UTF-8');
			
			include("mail/class.phpmailer.php");
			include("mail/class.smtp.php");
			
  
			$nombre = $_POST["nombre"];
			$correo = $_POST["correo"];
			$mensaje = $_POST["mensaje"];
			$contenido = '';
        
			$fecha_registro = date("Y-m-d H:i:s");
      
			$contenido .= "<html>
					<head>
						<meta charset='UTF-8'>
						<style>
							*{
								font-family: Arial;
							}
							td{
								border: 1px solid #ddd;
							}
							table{
								background: #f1f1f1;
								border-collapse: collapse;
							}
						</style>
					</head>
					<body>
						<table width='600' align='center' valign='center' cellpadding='10'>
							<tr>
								<td colspan='2'><img src='https://www.tufuturoencanada.com/img/Logo_TuVidaEnCanada.png'></td>
							</tr>

							<tr>
								<td>Nombre: </td><td>".$nombre."</td>
							</tr>
							<tr>
								<td>Correo: </td><td>".$correo."</td>
							</tr>
							<tr>
								<td>Mensaje: </td><td>".$mensaje."</td>
							</tr>

						</table>
					</body>
				</html>";
          
           
         
			
         $mail = new PHPMailer(); 
         
         
         
			//$mail->IsSMTP();
			//$mail->SMTPAuth = true;
			//$mail->Host = "titan.hosting-mexico.net";
			//$mail->Port = 587;
			//$mail->Password = 'WExtNEo}3T}M'; 
			//$mail->Username = "info@grupocob.com";
			$mail->From = "info@tufuturoencanada.com";
			$mail->FromName = "Formulario de contacto web";
			$mail->addAddress("info@tufuturoencanada.com");
			$mail->AddBCC("aalvarado@bitaminadigital.com"); 
			$mail->AddBCC("testposicionart@gmail.com"); 
			$mail->IsHTML(true);
			//$contenido = utf8_decode($contenido);
			$mail->Subject = "Correo enviado desde formulario de Contacto";
         $mail->Body = $contenido;
         
        
			if($mail->Send()){
            header('Location: '.$httpProtocol.$host.$url.'gracias');
            die();
			}

			header('Location: '.$httpProtocol.$host.$url.'contacto');
		}else{
			$_SESSION["error"] = 1;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
	}
?>
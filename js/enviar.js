function sendEmailContact(){
    // FunciÃ³n para checar que no estÃ©n vacÃ­os los campos
    var checkIsNotEmpty = function(auxiliar, name){
        if(auxiliar.value == '' || auxiliar.value == null){
			let msg = 'Por favor completa el campo de: ' + name;
            messageError(msg);
            return false;
		}
        return true;
    }
    // FunciÃ³n para checar si un campo tiene nÃºmeros en su strings
    var checkHasNoNumbers = function(auxiliar, name){
        if(/\d/.test(auxiliar.value)){
            let msg = 'Por favor remueve los números del campo de: ' + name;
            messageError(msg);
            return false;
		}
        return true;
    }

    var checkEmail = function(auxiliar, name){
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,4}/.test(auxiliar.value)){
			let msg = 'Porfavor ingresa un correo valido ' + name;
            messageError(msg);
            return false;
        }
        alert('todo bien');
        return true;
    }
    // FunciÃ³n que agrega el mensaje de error a mostrar
    var messageError = function(msgErr){
        $('#message_item').html(msgErr);
        $('.alert').alert();
        $('.alert').css('z-index','1001');
        $('.alert').css('position','fixed');
        $('.alert').css('bottom','50%');
        $('.alert').css('left','30%');
        $('.alert').css('right','30%');
        $(".alert").show().delay(200).addClass("in").fadeOut(3500);
        $('.alert').on('closed.bs.alert', function () {
            $('.alert').alert('dispose');
        });
    }
    
    // Obtenemos la informaciÃ³n guardada en el form de contacto
    var formulario = document.getElementById('form-contacto'),
    nombre = formulario.nombre,
    correo = formulario.correo,
    mensaje = formulario.mensaje;

    var flag = 0;
    // Checar que ningÃºn campo del form estÃ© vacÃ­o o incorrecto
    if(checkIsNotEmpty(nombre,'Nombre') && checkHasNoNumbers(nombre,'Nombre')){
        if(checkIsNotEmpty(correo,'Correo Electronico') ){
            if(checkIsNotEmpty(mensaje,'Mensaje')){
                // Flag para saber si ejecutar o no el envÃ­o de formulario    
                var flag = 1;
            }
        }
    }
    // Si la bandera es 1 es porque paso todas las validaciones
    if(flag == 1){
        return true;
    }else{
        return false;
    }
}
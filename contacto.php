
   <?php 
        $title =  'Contacto para el foro tu futuro en Canadá';
        $description = 'Contacto para el Foto Tu Futuro en Canadá. Tel. 55 7667 5372.' ;
        $keywords = 'tu futuro en canadá, estudiar en canadá, trabajar en canadá, vivir en canadá, migrar a canadá';
        include('header.php');
   ?>
<script src="js/enviar.js"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LfFycIUAAAAAAiXg7wkLFCUCNSnIAY4V9GbXgbF"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute("6LfFycIUAAAAAAiXg7wkLFCUCNSnIAY4V9GbXgbF", {action: "sendEmailContact"}).then(function(token) {
            var recaptchaResponse = document.getElementById("recaptchaResponse");
            recaptchaResponse.value = token;
        });
    });
</script>



<section class="main bgLightGray">
    <div class="relative">
        <img class="w-100" src="img/titulo-contacto.jpg" alt="titulo contacto">
        <h1 class="red absolute somosQ col-md-6 offset-md-3">CONTACTO</h1>
    </div>

    <div class="col-md-10 offset-md-1 pt-5">
    <div style="display:none;" class="alert alert-danger alert-dismissible fade show text-center" role="alert">
    <p id="message_item"></p>
</div>
        <div class="row pt-5">
      
            <div class="col-md-4">
                <h3>LLÁMANOS O ESCRÍBENOS</h3>
                <p class="pt-5"><a id="phone" href="tel:5576675372">Teléfono y WhatsApp 55 7667 5372</a></p>
                <p>Correo: <a id="mail" href="mailto:info@tufuturoencanada.com">info@tufuturoencanada.com</a></p>
            </div>
            <div class="col-md-6 offset-md-2">
                <h3>DÉJANOS UN MENSAJE</h3>
                <form action="mailer" id="form-contacto" class="formulario" method="POST" onsubmit="return sendEmailContact();">
                    <div class="form-group">   
                        <input type="text" placeholder="Nombre" name="nombre" id="nombre" class="form-control">                    
                    </div>
                    <div class="form-group">
                        <input type="email" placeholder="Correo" name="correo" id="correo" class="form-control"  pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,4}">                    
                    </div>
                    <div class="form-group">
                        <textarea  cols="30" rows="10" placeholder="Mensaje" name="mensaje" id="mensaje" class="form-control"></textarea>
                    </div>
                    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                    <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-danger submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>
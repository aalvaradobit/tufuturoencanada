
   <?php 
        $title =  'Registro al foto tu futuro a en Canadá';
        $description = '¿Quieres saber si eres candidato para estudiar, trabajar o migrar en Canadá? ¡La oportunidad de tu vida está muy cerca!' ;
        $keywords = 'tu futuro en canadá, estudiar en canadá, trabajar en canadá, vivir en canadá, migrar a canadá';
        include('header.php');
   ?>

<section class="main">
    <div class="relative">
        <img class="w-100" src="img/titulo-registro.jpg" alt="titulo registro">
        <h1 class="white absolute somosQ col-md-6 offset-md-3">registro</h1>
    </div>
</section>
<div class="col-md-8 offset-md-2">

<div class="os_widget" data-of="hec2" data-path="/hec2/quieres-saber-si-eres-candidato-para-vivir-en-canad%C3%A1" id="os-widget-566136"></div>


</div>
<?php include('footer.php'); 

?>

<script type="text/javascript">
(function(d,s,id,u){
  if (d.getElementById(id)) return;
  var js, sjs = d.getElementsByTagName(s)[0],
      t = Math.floor(new Date().getTime() / 1000000);
  js=d.createElement(s); js.id=id; js.async=1; js.src=u+'?'+t;
  sjs.parentNode.insertBefore(js, sjs);
}(document, 'script', 'os-widget-jssdk', 'https://www.opinionstage.com/assets/loader.js'));
</script>

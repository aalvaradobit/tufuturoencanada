
   <?php 
        $title =  'Gracias';
        $description = '' ;
        $keywords = '';
        include('header.php');
   ?>

<section class="main bgLightGray">
    <div class="relative">
        <img class="w-100" src="img/titulo-contacto.jpg" alt="">
        <h1 class="red absolute somosQ col-md-6 offset-md-3">GRACIAS</h1>
    </div>

    <div class="col-md-8 offset-md-2 text-center pt-5 pb-5">
        <div class="gracias">
           <p> Gracias por ponerse en contacto.</p>
            <p>Alguien se pondrá en contacto con usted pronto.</p>

            <a href="https://www.tufuturoencanada.com/">Ir al Inicio</a>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>
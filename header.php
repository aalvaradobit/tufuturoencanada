<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?> | Tu Futuro en Canadá</title>

    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="author" content="Bitamina Digital">

    <!--Metas Geo-->
    <meta name="geo.region" content="MX-DIF" />
    <meta name="geo.placename" content="Ciudad de M&eacute;xico" />
    <meta name="geo.position" content="19.395216;-99.175457" />
    <meta name="ICBM" content="19.395216, -99.175457" />


        <!--Metas OG-->
    <meta property="og:locale" content="es_MX" />
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:description" content="<?php echo $description; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://<?php echo $_SERVER["HTTP_HOST"]?>"/>
    <meta property="og:image" content="" />
    <meta property="og:site_name" content="Tu Futuro en Canadá" />

    <!--Metas DC-->
    <meta content="<?php echo $title; ?>" NAME="DC.Title"/>
    <meta content="<?php echo $description; ?>" NAME="DC.Description"/>
    <meta content="Bitamina Digital" NAME="DC.Creator"/>
    <meta content="TufuturoenCanadá" NAME="DC.Publisher"/>
    <meta content="<?php echo $description; ?>" NAME="DC.Identifier"/>
    <meta content="<?php echo $keywords; ?>" NAME="DC.keywords"/>	
    
    <link rel="alternate" hreflang="x-default" href="https://<?php echo $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];?>">

    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#ff0000">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">

    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56239256-2"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56239256-2');
        </script>

    
    <script src="js/all.js"></script>


    <link rel="stylesheet" href="css/all.css">
 

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script.js"></script>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-light bgTransparent">
        <a href="/"><img src="img/Logo_TuVidaEnCanada.png" alt="Logo TuVidaEnCanada"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto pr-5">
            <li class="nav-item pl-3">
                <a class="nav-link" id="index" href="/">HOME </a>
            </li>
            <li class="nav-item long pl-3">
                <a class="nav-link " id="quienes" href="quienes-somos">QUIÉNES SOMOS </a>
            </li>
            <li class="nav-item pl-3">
                <a class="nav-link" id="registro" href="registro">REGISTRO </a>
            </li>
            <li class="nav-item pl-3">
                <a class="nav-link" id="contacto" href="contacto">CONTACTO </a>
            </li>
            </ul>
        </div>
        </nav>
</header>
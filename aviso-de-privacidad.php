
   <?php 
        $title =  'Aviso de Privacidad';
        $description = '' ;
        $keywords = '';
        include('header.php');
   ?>
 <meta name="robots" content="noindex, nofollow">
<section class="main">
    <div class="relative">
        <img class="w-100" src="img/titulo-registro.jpg" alt="titulo registro">
        <h1 class="white absolute avisoP col-md-8 offset-md-2">Aviso de Privacidad</h1>
    </div>
</section>
<div class="col-md-8 offset-md-2 pt-5 avisoPrivacidad">


<p>De acuerdo a lo Previsto en la “Ley Federal de Protección de Datos Personales”, declara Intercambios Educativos al Extranjero SAPI de CV, ser una empresa legalmente constituida de conformidad con las leyes mexicanas, con domicilio en Avenida Canal de Miramontes #2700, Col. Los Cipreses, CP: 04830, CDMX; y como responsable del tratamiento de sus datos personales, hace de su conocimiento que la información de nuestros clientes es tratada de forma estrictamente confidencial por lo que al proporcionar sus datos personales, tales como:</p>

<ul>
    <li> Nombre Completo</li>
    <li> Teléfonos de Hogar o celular</li>
    <li> Correo Electrónico</li>
</ul>




<p>Estos datos serán utilizados única y exclusivamente para los siguientes fines:</p>

<ul class="numeral">
    <li>  Información y Prestación de Servicios.</li>
    <li>  Actualización de la Base de Datos.</li>
    <li>  Cualquier finalidad análoga o compatible con las anteriores.</li>
    <li>  Atención Personalizada.</li>
</ul>



<p>
    Intercambios Educativos al Extranjero SAPI de CV no solicita datos personales sensibles en su sitio web <a href="https://www.tufuturoencanada.com/">https://www.tufuturoencanada.com/</a>
</p>
<p>
    Para prevenir el acceso no autorizado a sus datos personales y con el fin de asegurar que la información sea utilizada para los fines establecidos en este aviso de privacidad, hemos establecido diversos procedimientos con la finalidad de evitar el uso o divulgación no autorizados de sus datos, permitiéndonos tratarlos debidamente.
</p>

<p>
    En nuestro programa de notificación de promociones, ofertas y servicios a través de correo electrónico, sólo Softelligence tiene acceso a la información recabada. Este tipo de publicidad se realiza mediante avisos y mensajes promocionales de correo electrónico, los cuales sólo serán enviados a usted y a aquellos contactos registrados para tal propósito, esta indicación podrá usted modificarla en cualquier momento.
</p>

<p>
    Todos sus datos personales son tratados de acuerdo a la legislación aplicable y vigente en el país, por ello le informamos que usted tiene en todo momento los derechos (ARCO) de acceder, rectificar, cancelar u oponerse al tratamiento que le damos a sus datos personales; derecho que podrá hacer valer a través del Área de Privacidad encargada de la seguridad de datos personales en el Teléfono desde el <a href="tel:5576675372">55 7667 5372</a> ó por medio de su correo electrónico: <a href="mailto:info@tufuturoencanada.com">info@tufuturoencanada.com</a>
</p>

<p>Su petición deberá ir acompañada de la siguiente información:</p>


<ul>
    <li>Nombre y domicilio del solicitante. </li>

    <li>Números telefónicos del solicitante. </li>

    <li>Correo electrónico. </li>

    <li>Carta solicitud, explicando los motivos de su solicitud. </li>

    <li>En su caso, documentación justificativa de la modificación u oposición solicitada. </li>
</ul>

<p>
    Tendremos un plazo máximo de 20 días hábiles para atender su petición y le informaremos sobre la procedencia de la misma los teléfonos que nos proporcione y/o a su correo electrónico.
</p>

<p>
    Este aviso de privacidad podrá ser modificado por <a href="https://www.tufuturoencanada.com/">https://www.tufuturoencanada.com/</a> dichas modificaciones serán oportunamente informadas a través de correo electrónico, vía telefónica o en el sitio web <a href="https://www.tufuturoencanada.com/">https://www.tufuturoencanada.com/</a>
</p>

<p>
    Si el usuario utiliza el sitio web de Tu futuro en Canadá, significa que ha leído, entendido y acordado los términos antes expuestos. Si no está de acuerdo con ellos, el usuario no deberá proporcionar ninguna información personal.
</p>

<p>ATENTAMENTE <br>
INTERCAMBIOS EDUCATIVOS AL EXTRANJERO
</p>


</div>
<?php include('footer.php'); 

?>



   <?php 
        $title =  'Tu futuro en Canadá, el foto para vivir y estudiar en Canadá';
        $description = 'Tu Futuro en Canadá es el Foto donde expertos y especialistas te darán la información necesarias para estudiar, vivir o migrar a Canadá.' ;
        $keywords = 'tu futuro en canadá, estudiar en canadá, trabajar en canadá, vivir en canadá, migrar a canadá';
        include('header.php');
   ?>
    
    <section class="main">
        <div class="firstDiv">
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active relativeRes">
                    <img class="d-block w-100" src="img/B1-tu-futuro-en-Canada.jpg" alt="First slide">
                    <div class="absoluteRes eventoTxt col-md-10"> 
                        <h2 class="pt-5"><span>EL FORO EN DONDE </span> </h2>
                        <h2 class="pb-5"> DESCUBRIRÁS <span>CÓMO</span></h2>
                            <ul class="pl-5 pb-5">
                                <li>Estudiar</li>
                                <li>Trabajar</li>
                                <li>Migrar <small>a Canadá</small></li>
                            </ul>
                           <div class="col-md-10 ml-auto d-block">
                                <a href="registro" class="btn btn-warning ">Descubre si eres candidato</a>
                           </div>
                           <p class="red pt-5">Sábado</p>
                            <p class="red">7 de diciembre 2019</p>
                            <p><strong>10:00 am</strong></p>
                           <p> <small>Crown Plaza WTC</small></p>
                    </div>
                  </div>
                  <!-- <div class="carousel-item relativeRes">
                    <img class="d-block w-100" src="img/B2-tu-futuro-en-Canadá.jpg" alt="Second slide">
                  </div> -->
                </div>
              </div>
        </div>

        <div>
        <p class="expoRedTitle text-center pt-5">EXPOSITORES</p>
        <p class="expoYellowTitle text-center pb-3">DE LUJO</p>
        </div>
        
        <div class="expositoresDiv">
            <div class="col-md-8 offset-md-2">
                <img class="w-100" src="img/B2-tu-futuro-en-Canada.png" alt="Expositores">
                <div class="expo1 absoluteRes">
                    <h2 class="text-right" style="padding-right: 20%;">JULIO <span class="red">LOZOYA</span></h2>
                    <p class="text-right red"><img class="w-100" src="svg/Linea_Lozoya.svg" alt="Linea Lozoya"></p>
                
                    <p class="pb-2" style="padding-right: 35%;">Expero en <span class="red">marketing</span>, vendedor en activo, <span class="red">TED TALK Speaker</span>, docente en activo a nivel maestria y Blog Entreprenur <span class="red">Life Style</span></p>
                    <span class="yellowSpan">CONFERENCISTA</span>
                    <p class="pt-2">de alto nivel.</p>
                </div>
                <div class="expo2 absoluteRes">
                    <h2 class="">ISABEL <span class="red">BARTHALIS</span></h2>
                    <p class="red"><img class="w-100" src="svg/Línea_Isabel.svg" alt="LineaIsabel"></p>
                  
                    <p class="pb-2">Empresaria, Experta en <span class="red">marketing</span> y expositora en eventos de <span class="red">turismo educativo</span>.</p>
                    <span class="yellowSpan">CONFERENCISTA</span>
                    <p class="pt-2">especialista en estudios al extrangero.</p>
                </div>
                <div class="expo3 absoluteRes">
                    <h2 class="">J CARLOS <span class="red">HERNÁNDEZ</span></h2>
                    <p class="red"><img class="w-100" src="svg/Línea_JCarlos.svg" alt="LineaCarlos"></p>
                    <p class="pb-2">Especialista vinculado  desde hace  10 años al <span class="red">turismo educativo </span> internacional en Latinoamérica. Conferencista  especializado en <span class="red">experiencias </span> educativas en el <span class="red">exterior.</span></p>
                    <span class="yellowSpan">CONFERENCISTA</span>
                    <p class="pt-2">especializado en turismo</p>
                </div>
            </div>
        </div>

        <div class="expositoresDivResponsive">
            <div class="col-md-10 offset-md-1">
                <div class="expoRes1 row">
                    <div class="col-md-6 col-6">
                        <img src="img/Lonzoya.png" class="w-100 pt-5" alt="Lonzoya">
                    </div>
                    <div class="col-md-6 col-6">
                     <h2>JULIO <span class="red">LOZOYA</span></h2>
                        <p class="red"><img class="w-100" src="svg/Linea_Lozoya.svg" alt="LineaLonzoya"></p>
                    
                        <p class="pb-2">Expero en <span class="red">marketing</span>, vendedor en activo, <span class="red">TED TALK Speaker</span>, docente en activo a nivel maestria y Blog Entreprenur <span class="red">Life Style</span></p>
                        <span class="yellowSpan">CONFERENCISTA</span>
                        <p class="pt-2">de alto nivel.</p>
                    </div>
                </div>
                <div class="expoRes2 row">
                    <div class="col-md-6 col-6">
                        <h2 class="">ISABEL <span class="red">BARTHALIS</span></h2>
                        <p class="red"><img class="w-100" src="svg/Línea_Isabel.svg" alt="Conferencista1"></p>
                    
                        <p class="pb-2">Empresaria, Experta en <span class="red">marketing</span> y expositora en eventos de <span class="red">turismo educativo</span>.</p>
                        <span class="yellowSpan">CONFERENCISTA</span>
                        <p class="pt-2">especialista en estudios al extrangero.</p>
                    </div>

                    <div class="col-md-6 col-6">
                        <img src="img/Isabel.png" class="w-100 pt-5" alt="Conferencista2">
                    </div>
                </div>
                <div class="expoRes3 row">
                    <div class="col-md-6 col-6">
                        <img src="img/JCarlos.png" class="w-100 pt-5" alt="Conferencista3">
                    </div>
                  <div class="col-md-6 col-6">
                    <h2>J CARLOS <span class="red">HERNÁNDEZ</span></h2>
                        <p class="red"><img class="w-100" src="svg/Línea_JCarlos.svg" alt="Linea JCarlos"></p>
                        <p class="pb-2">Especialista vinculado  desde hace  10 años al <span class="red">turismo educativo </span> internacional en Latinoamérica. Conferencista  especializado en <span class="red">experiencias </span> educativas en el <span class="red">exterior.</span></p>
                        <span class="yellowSpan">CONFERENCISTA</span>
                        <p class="pt-2">especializado en turismo</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="secondDiv pt-5">
            <div class="col-md-10 offset-md-1">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <div class="top pb-5">
                            <h2 class="text-center">¿QUÉ ES TU FUTURO EN CANADÁ?</h2>
                            <p>Es un foro en donde podrás tener acceso a toda la información necesaria para vivir, estudiar y migrar a Canadá impartida por expertos en la materia. Descubrirás que la manera más fácil de migrar es a través del estudio de un posgrado.</p>
                        </div>
                        <div class="bottom pt-5">
                            <h2 class="text-center">¿QUIÉNES HACEN POSIBLE TU FUTURO EN CANADÁ?</h2>
                            <p>Instituciones Educativas Canadienses públicas y privadas que cuentan con programas de posgrado que brindan la posibilidad de migrar a Canadá cuando el estudiante cuenta con el perfil y cumple con los requisitos que dicta el gobierno canadiense.</p>
                        </div>
                    </div>
                    <div class="col-md-4 pb-5">
                        <img src="img/Imagen_Joven.png" alt="Imagen Joven" class="w-100">
                    </div>
                    <div class="col-md-4">
                        <div class="top pb-5">
                            <h2 class="text-center">¿POR QUÉ ES IMPORTANTE TU REGISTRO A UNO DE LOS PROGRAMAS DURANTE EL EVENTO?</h2>
                            <p>Cómo sabes el proyecto de vivir, estudiar y migrar a Canadá es un proceso que requiere de tiempo y del cumplimiento de varios requisitos. Es por esto que además de obtener beneficios económicos, las instituciones darán prioridad a los candidatos que se inscriban a uno de sus programas educativo el día del evento.</p>
                        </div>
                        <div class="bottom pt-5">
                            <h2 class="text-center">¿QUÉ INCLUYE TU ENTRADA AL EVENTO?</h2>
                            <p>Tu entrada a Tu Futuro a Canadá además de tener acceso al foro, tendrás derecho a agendar a una asesoría personalizada para conocer el programa que mejor se adapta a tus expectativas.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="thirdDiv pt-5">
            <div class="col-md-10 offset-md-1 col-10 offset-1 text-center">
                <div class="row no-gutters">
                    <div class="col-md-3 col-6">
                        <img class="pl-3 pr-3" src="img/Logo_Camosun.jpg" alt="Camosum">
                    </div>
                    <div class="col-md-3 col-6">
                        <img class="pl-3 pr-3" src="img/caledonia-200x200.jpg" alt="Caledonia">
                    </div>
                    <div class="col-md-3 col-6">
                        <img class="pl-3 pr-3" src="img/humer-200x200.jpg" alt="Humer">
                    </div>
                    <div class="col-md-3 col-6">
                        <img class="pl-3 pr-3" src="img/st.-lawrence-200x200.jpg" alt="St Lawrence">
                    </div>

                    <div class="w-20 responsiveLogo">
                        <img class="pl-2 pr-2" src="img/canacian-college-200x200.jpg" alt="Canadian College">
                    </div>
                    <div class="w-20 responsiveLogo">
                        <img class="pl-2 pr-2" src="img/canada-west-200x200.jpg" alt="Canada West">
                    </div>
                    <div class="w-20 responsiveLogo">
                        <img class="pl-2 pr-2" src="img/ilac-200x200.jpg" alt="ILAC">
                    </div>
                    <div class="w-20 responsiveLogo">
                        <img class="pl-2 pr-2" src="img/tamwood-200x200.jpg" alt="Tamwood">
                    </div>
                    <div class="w-20 responsiveLogo">
                        <img class="pl-2 pr-2" src="img/EduInter-200x200.jpg" alt="EduInter">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('footer.php'); ?>